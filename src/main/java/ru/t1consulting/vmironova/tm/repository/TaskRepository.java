package ru.t1consulting.vmironova.tm.repository;

import ru.t1consulting.vmironova.tm.api.repository.ITaskRepository;
import ru.t1consulting.vmironova.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.vmironova.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public List<Task> findAll(final Comparator comparator) {
        final List<Task> result = new ArrayList<>(tasks);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> projectTasks = new ArrayList<>();
        for (final Task task : tasks) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) projectTasks.add(task);
        }
        return projectTasks;
    }

    @Override
    public Task create(final String name, final String description) {
        return add(new Task(name, description));
    }

    @Override
    public Task create(final String name) {
        return add(new Task(name));
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        tasks.remove(task);
        return task;
    }

    @Override
    public int getSize() {
        return tasks.size();
    }

}
