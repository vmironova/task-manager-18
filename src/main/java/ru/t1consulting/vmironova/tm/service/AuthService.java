package ru.t1consulting.vmironova.tm.service;

import ru.t1consulting.vmironova.tm.api.service.IAuthService;
import ru.t1consulting.vmironova.tm.api.service.IUserService;
import ru.t1consulting.vmironova.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.vmironova.tm.exception.field.LoginEmptyException;
import ru.t1consulting.vmironova.tm.exception.field.PasswordEmptyException;
import ru.t1consulting.vmironova.tm.exception.user.AccessDeniedException;
import ru.t1consulting.vmironova.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1consulting.vmironova.tm.model.User;
import ru.t1consulting.vmironova.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = userService.findById(userId);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

}
