package ru.t1consulting.vmironova.tm.command.user;

import ru.t1consulting.vmironova.tm.model.User;
import ru.t1consulting.vmironova.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "User registry.";

    public static final String NAME = "user-registry";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final User user = getAuthService().registry(login, password, email);
        showUser(user);
    }

}
