package ru.t1consulting.vmironova.tm.command.task;

import ru.t1consulting.vmironova.tm.model.Task;
import ru.t1consulting.vmironova.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Show task by index.";

    public static final String NAME = "task-show-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = getTaskService().findOneByIndex(index);
        showTask(task);
    }

}
