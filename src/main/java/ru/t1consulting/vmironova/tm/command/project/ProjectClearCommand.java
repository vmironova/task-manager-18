package ru.t1consulting.vmironova.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Remove all projects.";

    public static final String NAME = "project-clear";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        getProjectService().clear();
    }

}
