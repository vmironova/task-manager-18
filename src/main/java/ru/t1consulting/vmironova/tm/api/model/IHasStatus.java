package ru.t1consulting.vmironova.tm.api.model;

import ru.t1consulting.vmironova.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
