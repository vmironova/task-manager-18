package ru.t1consulting.vmironova.tm.api.repository;

import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.model.User;

import java.util.List;

public interface IUserRepository {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User add(User user);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    Boolean isLoginExists(String login);

    Boolean isEmailExists(String email);

}
